///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01 - Hello World
//
// @author Baishen Wang <baishen@hawaii.edu>
// @date   16 Jan 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>

int main() {
	printf("Hello world!\n");
   
   printf("I am Sam\n");
}

